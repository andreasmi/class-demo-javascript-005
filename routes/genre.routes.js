const router = require('express').Router();
const db = require('../db.config');
const genreFactory = require('../factories/genre');

// Get genres
router.get('/', (req, res) => {
    const genres = genreFactory(db).getAll();
    return res.status(200).json({
        success: true,
        data: genres
    });
});

// Get a specific genres with ID
router.get('/:genreId', (req, res) => {
    const { genreId } = req.params;
    const genre = genreFactory(db).getBy('id', genreId);
    return res.status(200).json({
        success: true,
        data: genre
    });
});

// Post a new genre
router.post('/', (req, res) => {

    const { genre } = req.body;

    if (!genre) {
        return res.status(404).json({
            success: false,
            error: 'No genre was sent.'
        });
    }

    const { data, error = false } = genreFactory(db).createGenre(genre);

    if (error !== false) {
        return res.status(409).json({
            success: false,
            error
        });
    }

    return res.status(201).json({
        success: true,
        data
    });

});

router.delete('/:genreId', (req, res) => {
    const { genreId } = req.params;
    genreFactory(db).delete(genreId);
    return res.status(410).json({
        success: true
    });
});

module.exports = router;