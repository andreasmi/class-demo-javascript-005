const { getterFactory, setterFactory } = require('./db');
const { v4: uuidv4 } = require('uuid');
const { passwordHasher, passwordValidator } = require('../utils/passwords');

const userAuthFactory = state => ({
    login: (username, password) => {

        const userInDb = state.db.get(state.table).find({ username }).value();

        if (!userInDb) {
            return {
                error: `${username} not found`
            };
        }

        if (!passwordValidator(userInDb.hashData, password)) {
            return {
                error: `Incorrect login credentials...`
            };
        }

        state.db.get(state.table)
            .find({ username: username.toLowerCase() })
            .assign({ lastLogin: new Date().getTime() })
            .write();

        return {
            success: true,
            data: userInDb.id
        };
    },
    register: (username, password) => {

        const existsInDb = getterFactory(state).getBy('username', username.toLowerCase()).value();

        if (existsInDb && existsInDb.username) {
            return {
                error: `The user ${username} already exists...`
            };
        }

        const hashData = passwordHasher( password );
        
        const userId = setterFactory(state).create({
            username: username.toLowerCase(),
            hashData,
            favourites: [],
            createdAt: new Date().getTime(),
            lastLogin: new Date().getTime()
        });

        return {
            success: true,
            data: userId
        };
    }
});

const favouritesFactory = state => ({
    getUserFavourites: userId => {
        const { favourites = [] } = getterFactory(state).getBy('id', userId).value();
        const favouriteMovies = favourites.map(movieId => {
            return state.db.get('movies').find({ id: movieId }).value()
        }) || [];
        return favouriteMovies;
    },
    addUserFavourite: (userId, movieId) => {
        const { favourites = [] } = getterFactory(state).getBy('id', userId).value();

        if (favourites.includes( movieId )) {
            return {
                error: `The movie is already in your favourites`
            }
        }

        getterFactory(state)
            .getBy('id', userId)
            .assign({
                favourites: [...favourites, movieId]
            })
            .write();
            return {
                success: true,
                data: [...favourites, movieId]
            };
    }
});

const users = uuid => db => {

    const state = {
        uuid,
        db,
        table: 'users'
    };

    return Object.assign(
        {},
        getterFactory(state),
        userAuthFactory(state),
        favouritesFactory(state)
    );
}

module.exports = users(uuidv4);