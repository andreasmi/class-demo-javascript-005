const express = require('express');
const app = express();
const { PORT = 3000 } = process.env;
const cors = require('cors');

// Middleware
app.use(express.json());
app.use( cors() );

// Base Endpoint.
app.get('/', (req, res) => {
    return res.status(200).send('Im a fancy Movie API.');
});

// Import routes from ./routes/*
app.use('/v1/movies', require('./routes/movie.routes'));
app.use('/v1/genres', require('./routes/genre.routes'));
app.use('/v1/users', require('./routes/user.routes'));

app.listen(PORT, () => console.log('Server started on port ' + PORT));